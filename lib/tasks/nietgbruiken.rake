namespace :update_products do
desc "update products"
task :zanox => :environment do
  require 'net/http'
  require 'json'
  require 'open-uri'
   products = Product.all


  #    url = "https://api.bol.com/catalog/v4/search/?q=#{product.title}&limit=1&apikey=#{apikey}&format=json"
  #    uri = URI(url)
  #    response = Net::HTTP.get(uri)
  #    result =JSON.parse(response)
  #    url = "https://partnerprogramma.bol.com/click/click?p=1&t=url&s=43445&&url=#{result['products'][0]['urls'][0]['value']}"
  #    product.amounts.create(price: result['products'][0]['offerData']['offers'][0]['price'],shop: "Bol.com", url: url )
  #    puts "adding prices to #{product.title}.... "
  # end


  connectid = "A98EB1749EBA063D5EB0"
  products.each do |product|
    url = "http://api.zanox.com/json/2011-03-01/products?connectid=#{connectid}&ean=#{product.ean}&region=NL"
    uri = URI(url)
    response = Net::HTTP.get(uri)
    result =JSON.parse(response)
    price = ""
    shop = ""
    url = ""
    name = ""

    if result['productItems'].blank?
      puts "result is nil.....adding old prices"
      product.amounts.create(price: product.amounts.last.price ,shop: product.amounts.last.shop, url: product.amounts.last.url )
   elsif "#{result['productItems']['productItem'][0]['ean']}".include?("#{product.ean}")         #/#{result['productItems']['productItem'][0]['name']}/i.match(product.title)
      price = result['productItems']['productItem'][0]['price']
      shop = result['productItems']['productItem'][0]['program']['$']
      url = result['productItems']['productItem'][0]['trackingLinks']['trackingLink'][0]['ppc']
      product.amounts.create(price: price ,shop: shop, url: url )
      puts "adding prices to #{product.title}.......#{result['productItems']['productItem'][0]['name']} "
    else
      product.amounts.create(price: product.amounts.last.price ,shop: product.amounts.last.shop, url: product.amounts.last.url )
      puts "no match.....adding old prices"
      puts result['productItems']['productItem'][0]['name']
      puts product.title
      p  /result['productItems']['productItem'][0]['name']/i.match(product.title)
    end
  end
end

desc "update products"
task :bol => :environment do
  apikey = "41E4F374DEAA4E3F9CF54246B7253D6F"
  bol = Product.all
  bol.each do |item|
      url = "https://api.bol.com/catalog/v4/search/?q=#{item.ean}&limit=1&apikey=#{apikey}&format=json"
      uri = URI(url)
      response = Net::HTTP.get(uri)
      result =JSON.parse(response)

      price = ""
      shop = ""
      url = ""
      name = ""
      #puts result['products']
      #  puts res['products'][0]['offerData']['offers'][0]['price'] #fetch price
      # # puts res['products'][0]['urls'][0]['value']
      # puts res['products'][0]['urls'][0]['value'] #fetch first url
      brandmodel = ""
      brandmodel = "".downcase
      branditle = ""
      branditle = ""

      if result['products'].blank?
        item.amounts.create(price: item.amounts.last.price ,shop: item.amounts.last.shop, url: item.amounts.last.url )
        puts "No Result......adding old prices"
        puts result['products']
        puts item.title
      elsif  "#{result['products'][0]['ean']}".downcase.include?("#{item.ean}")  #/#{brandmodel}/i.match(branditle)
        price = result['products'][0]['offerData']['offers'][0]['price']
        shop = "Bol.com"
        url = result['products'][0]['urls'][0]['value']
        item.amounts.create(price: price ,shop: shop, url: url )
        puts "adding prices to #{item.brand.name}....#{item.product_model}......#{result['products'][0]['title']} "
      else
        item.amounts.create(price: item.amounts.last.price ,shop: item.amounts.last.shop, url: item.amounts.last.url )
        puts "#{result['products'][0]['title']}".downcase.include?("#{item.brand.name}  #{item.product_model}".downcase)
        puts brandmodel.downcase
        puts "#{result['products'][0]['title']}".downcase
        puts "No Match.... adding old prices"
        p /#{result['products'][0]['title']}/i.match(brandmodel)
      end
    end
end





desc "update products"
task :wehkamp => :environment do
  apikey = "41E4F374DEAA4E3F9CF54246B7253D6F"
  doubler = Product.all
  doubler.each do |item|
      url = URI::encode("http://api.tradedoubler.com/1.0/products.json;model=#{item.product_model};brand=#{item.brand.name};q=#{item.title}?token=#{apikey}")
      uri = URI(url)
      response = Net::HTTP.get(uri)
      result =JSON.parse(response)

      price = ""
      shop = ""
      url = ""
      name = ""
      #puts result['products']
      #  puts res['products'][0]['offerData']['offers'][0]['price'] #fetch price
      # # puts res['products'][0]['urls'][0]['value']
      # puts res['products'][0]['urls'][0]['value'] #fetch first url
      brandmodel = ""
      brandmodel = "".downcase
      branditle = ""
      # branditle = "#{result['products'][0]['name']}"

      if result['products'].blank?
        item.amounts.create(price: item.amounts.last.price ,shop: item.amounts.last.shop, url: item.amounts.last.url )
        puts "No Result......adding old prices"

        puts item.title
      elsif  "#{result['products'][0]['model']} #{result['products'][0]['brand']}".downcase.include?("#{item.product_model}".downcase) && "#{result['products'][0]['model']} #{result['products'][0]['brand']}".downcase.include?("#{item.brand.name}".downcase)  #/#{brandmodel}/i.match(branditle)
        price = result['products'][0]['offers'][0]['priceHistory'][0]['price']['value']
        shop = "wehkamp"
        url = result['products'][0]['offers'][0]['productUrl']
        item.amounts.create(price: price ,shop: shop, url: url )
        puts "adding prices to #{item.brand.name}....#{item.product_model}...... "
      else
        item.amounts.create(price: item.amounts.last.price ,shop: item.amounts.last.shop, url: item.amounts.last.url )
        # puts "#{result['products'][0]['title']}".downcase.include?("#{item.brand.name}  #{item.product_model}".downcase)
        # puts brandmodel.downcase
        # puts "#{result['products'][0]['title']}".downcase
        puts "No Match.... adding old prices"
        # p /#{result['products'][0]['title']}/i.match(brandmodel)
      end
    end
end





desc "update products"
task :tradedoubler => :environment do
  apikey = "6C674A0836F997222C5E61BC377B286C2B58C77C"
  doubler = Product.all
  doubler.each do |item|
      url = URI::encode("http://api.tradedoubler.com/1.0/products.json;fid=cf9e1ea1-c8fd-406c-89d5-93db027b5e57;model=#{item.product_model};brand=#{item.brand.name};q=#{item.title}?token=#{apikey}")
      uri = URI(url)
      response = Net::HTTP.get(uri)
      result =JSON.parse(response)

      price = ""
      shop = ""
      url = ""
      name = ""
      #puts result['products']
      #  puts res['products'][0]['offerData']['offers'][0]['price'] #fetch price
      # # puts res['products'][0]['urls'][0]['value']
      # puts res['products'][0]['urls'][0]['value'] #fetch first url
      brandmodel = ""
      brandmodel = "".downcase
      branditle = ""
      # branditle = "#{result['products'][0]['name']}"

      if result['products'].blank?
        item.amounts.create(price: item.amounts.last.price ,shop: item.amounts.last.shop, url: item.amounts.last.url )
        puts "No Result......adding old prices"

        puts item.title
      elsif  "#{result['products'][0]['model']} #{result['products'][0]['brand']}".downcase.include?("#{item.product_model}".downcase) && "#{result['products'][0]['model']} #{result['products'][0]['brand']}".downcase.include?("#{item.brand.name}".downcase)  #/#{brandmodel}/i.match(branditle)
        price = result['products'][0]['offers'][0]['priceHistory'][0]['price']['value']
        shop = result['products'][0]['offers'][0]['programName']
        url = result['products'][0]['offers'][0]['productUrl']
        item.amounts.create(price: price ,shop: shop, url: url )
        puts "adding prices to #{item.brand.name}....#{item.product_model}...... "
      else
        item.amounts.create(price: item.amounts.last.price ,shop: item.amounts.last.shop, url: item.amounts.last.url )
        # puts "#{result['products'][0]['title']}".downcase.include?("#{item.brand.name}  #{item.product_model}".downcase)
        # puts brandmodel.downcase
        # puts "#{result['products'][0]['title']}".downcase
        puts "No Match.... adding old prices"
        # p /#{result['products'][0]['title']}/i.match(brandmodel)
      end
    end
end


end


#{}"#{result['products'][0]['title']}".downcase.include?(brandmodel.downcase) #/#



#productItems"=>""

# require 'net/http'
# require 'json'
#
# apikey = "41E4F374DEAA4E3F9CF54246B7253D6F"
# search = ["Samsung UE40J6240",
#            "Salora 24LED1500 kopen",
#            "koop LG 43UH610V",
#            "Bosch PSB 850-2 RE boormachine kopen",
#            "Bosch PSB 530 RE boormachine kopen",
#            "Philips 32PFK5300",
#            "Samsung UE32K5600"]



# puts res['products'][0]['offerData']['offers'][0]['price']
# ['products']['title']

# search.each do |item|
#     url = "https://api.bol.com/catalog/v4/search/?q=#{item}&limit=1&apikey=#{apikey}&format=json"
#     uri = URI(url)
#     response = Net::HTTP.get(uri)
#     result =JSON.parse(response)
#      puts res['products'][0]['offerData']['offers'][0]['price'] #fetch price
#     # puts res['products'][0]['urls'][0]['value']
#     puts res['products'][0]['urls'][0]['value'] #fetch first url
#
# end
#puts res['products'][0]['title'] ##title
#    https://partnerprogramma.bol.com/click/click?p=1&t=url&s=43445&&url=
