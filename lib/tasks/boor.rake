namespace :boor do
  desc "boormachine"
  task :boor_spin => :environment do
    products = Product.all
    products.each do |product|
      if product.category.name.downcase == "boormachines"
        # Spin code comes here
        if product.boor_machines.any? && product.spin == nil
          spin1 = "<p>vergelijk de prijzen van de #{product.title} bij verschillende webshops. De juiste prijs vind je op VERGELIJK.TIPS ✅ veel webshops ✅ laagste prijzen ✅ bespaar geld. {Er zijn vele verschillen|Deze boormachine er|Je kunt hem|Dit is er een} in diverse uitvoeringen Kopen. <strong>#{product.title} #{product.category.name}</strong> Verschillende uitvoeringen en maten verkrijgbaar.".spin
          spin2 = "{Met een vermogen van|Dankzij |Door het vermogen van} #{product.boor_machines.last.vermogen} watt, {een pneumatisch| een geoliede| de soepele} #{product.brand.name} slagkracht, {een compact ergonomische design| slik ontwerp| een mooi vormgegeven ontwerp} en een gewicht van {slechts| ongeveer| } #{product.boor_machines.last.gewicht} kilo {is het wel| is het zeker|kun je} {heel erg prettig werken|gemakkelijk werken} met de #{product.title}. <p>".spin
          spin3 = "<p>{Of je deze #{product.boor_machines.last.type_boor}|Zelfs als je de #{product.boor_machines.last.type_boor}|De #{product.boor_machines.last.type_boor}} {nu inzet bij het| gebruikt bij het} boren van gaten met een {doorsnee van 22 millimeter| omvang vAN 33mm| grote van 55mm} {in beton| in hout| in steen| in gips} ".spin
          spin4 = "{of bij breekwerkzaamheden| of bij andere werkzaamheden} waarbij je gebruik maakt van de geïntegreerde beitelfunctie met Vario-Lock. {Dankzij de diepteaanslag| Door de aanslag} en de tweede handgreep werk je precies en met de juiste grip. {Je kunt overigens| er kan| u kunt trouwens} meteen aan de slag met de #{product.title}. ".spin
          spin5 = "{Een #{product.boor_machines.last.type_boor} | de #{product.boor_machines.last.type_boor}}, {soms ook bestempeld| af en toe ook } als SDS boormachine door het gebruik van deze booropnametechniek, {is geschikt voor het boren| kan gebruikt worden| kan worden ingezet} in harde steensoorten en hard beton. Een #{product.boor_machines.last.type_boor}".spin
          spin6 = "die ook voorzien is van een beitelfunctie wordt ook wel een combihamer genoemd. Anders dan bij een klopboormachine is een boorhamer uitgerust met een elektropneumatisch hamerwerk. Binnen in de machine zorgt luchtverplaatsing tussen een zuiger en slagpin voor schokgolven in de boor of beitel. {Het zijn dus de schokgolven| Dankzij de golven | door de schokgolven} in de beitel of boor van de pneumatische boorhamer die het grove werk verrichten, zonder dat ze schade aan de machine aanbrengen.</p>".spin
          spin7 = "<p>{De | Deze} #{product.title} { heeft een vermogen van| beschikt over een vermogen van } #{product.boor_machines.last.vermogen} watt, en {het type boormachine kan verschillen | het soort boormachine kan verschillen} dit is een #{product.boor_machines.last.soort_boorhouder}.".spin
          spin8 = "Accuschroefboormachines  {zijn niet voor niets populair| niet zomaar gewild| niet zonder reden gewild}, {je hebt geen gedoe met een snoer| het scheelt een hoop gedoe met snoer}, zijn redelijk tot behoorlijk krachtig, schroeven in draaien of het boren van gaten in hout gaat gemakkelijk, zelfs in steen en metaal. Als ik op zoek ga naar een nieuwe accuboormachine moet ik als eerste een beslissing nemen of ik alleen schroeven wil in- of uitdraaien of dat ik ook met de machine moet kunnen boren. ".spin
          spin9 = "<strong>De #{product.title} </strong>heeft een vermogen van ongeveer  #{product.boor_machines.last.toeren_per_minuut} omwentelingen per minuut. {Dit is een vrij normaal aantal| Niet een ongewoon aantal| Een vrij vaak voorkomend aantal} voor dit soort machines #{product.brand.name} boormachines zijn {erg stil| zonder veel geluid|met weinig lawaai| met weinig lawaai}.</p>".spin

          product.spin = " #{spin1.sample} #{spin2.sample} #{spin3.sample} #{spin4.sample} #{spin5.sample} #{spin6.sample} #{spin7.sample} #{spin8.sample} #{spin9.sample}"
          product.save
          puts product.spin

         end
      end
    end
  end
end
