namespace :wasmachine do
  desc "wasmachine"
  task :wasmachines_spin => :environment do
    products = Product.all
    products.each do |product|
      if product.category.name.downcase == "wasmachines"
        # Spin code comes here
        if product.washing_machines.any? && product.spin == nil
          spin1 = "<p>vergelijk de prijzen van de #{product.title} bij verschillende webshops. De juiste prijs vind je op VERGELIJK.TIPS ✅ veel webshops ✅ laagste prijzen ✅ bespaar geld. {Er zijn vele verschillen|Deze wasmachine er|Je kunt hem|Dit is er een} in diverse uitvoeringen Kopen. <strong>#{product.title} #{product.category.name}</strong> Verschillende uitvoeringen en maten verkrijgbaar.".spin
          spin2 = "{De|Deze|Dit is de} <strong>#{product.title} </strong> wasmachine {maakt het puur op gevoel doseren|doseren doen we niet| zorgt dat we niet meer doseren} van {wasmiddel|wasverzachter|schonmaakmiddel} of {wasmiddel|wasverzachter|schonmaakmiddel} {verleden tijd|iets van het verleden}.".spin
          spin3 = "{Dit is een |Deze machine is een|Het model } #{product.washing_machines.last.belading} van deze wasmachine {het is een|dit is een} #{product.washing_machines.last.bouw} machine.".spin
          spin4 = "{De speciale| Dankzij de mooie|Door de } trommelstructuur {gaat hij|is de omgang} extra zacht met je kostbare kledingstukken om. {Mocht je minder dan #{product.washing_machines.last.maximum_vulgewicht} kg wasgoed willen wassen|Als je geen #{product.washing_machines.last.maximum_vulgewicht} kg wil wassen|Geen behoefte om #{product.washing_machines.last.maximum_vulgewicht} kg} dan is dat geen enkel probleem.</p><p>".spin
          spin5 = "{De| Deze} wasmachine {beschikt|heeft} namelijk over de ActiveWater-technologie. {Deze mooie|Deze prachtige| Deze geweldige|De super| De fantastische }<strong>#{product.title} </strong>is #{product.washing_machines.last.hoogte} cm ".spin
          spin6 = "{hoog| in de hoogte} {en in de breedte is hij| de breedte is| breedte maat is} #{product.washing_machines.last.breedte} cm.".spin
          spin7 = "De diepte maat van {deze| de} #{product.brand.name} wasmachine is #{product.washing_machines.last.diepte} cm.".spin
          spin8 = "{Tijdens het wassen|Als je een wasbeurt draait|Waneer de wasmachine wast} {kan de| is het mogelijk dat de| bereikt de}  #{product.title} wasmachine een {geluidsniveau| geluidshoogte| lawaai niveau} van #{product.washing_machines.last.geluidsniveau} {db| decibel} bereiken.".spin
          spin9 = "{Deze mooie|Deze prachtige| Deze geweldige|De super| De fantastische } <strong>#{product.title} </strong> {kan een toerental van |heeft een toerental van} #{product.washing_machines.last.toeren_per_minuut} bereiken.".spin
          spin10 = "{Het vulgewicht van de| het maximum gewicht van de|Maximaal kan in de} #{product.title} is #{product.washing_machines.last.maximum_vulgewicht} kg.".spin
          spin11 = "{De|Deze| } #{product.title} {valt in de| is van} de energieklasse #{product.washing_machines.last.energie_klasse}.</p><p>".spin
          spin12 = "{Het is erg prettig| Het is fijn| Het is ook erg fijn} dat de #{product.title} wasmachine {erg stil| zonder veel geluid|met weinig lawaai| met weinig lawaai} centrifugeert.".spin
          spin13 = "{Uw was| Je was } {zal heerlijk ruiken |ruikt straks weer lekker| ruikt heerlijk} met de {vele|verschillende| diverse} programma’s op de #{product.title}</p>".spin

          product.spin = " #{spin1.sample} #{spin2.sample} #{spin3.sample} #{spin4.sample} #{spin5.sample} #{spin6.sample} #{spin7.sample} #{spin8.sample} #{spin9.sample} #{spin10.sample} #{spin11.sample} #{spin12.sample} #{spin13.sample}"
          product.save
          puts product.spin

         end
      end
    end
  end
end
