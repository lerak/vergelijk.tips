namespace :spin do
  desc "spin"
  task :koelkast_spin => :environment do
    products = Product.all
    products.each do |product|
      if product.category.name.downcase == "koelkasten"
        # Spin code comes here
        if product.refrigerators.any? && product.spin == nil
          spin1 = "<p>vergelijk de prijzen van de #{product.title} bij verschillende webshops. De juiste prijs vind je op VERGELIJK.TIPS ✅ veel webshops ✅ laagste prijzen ✅ bespaar geld. {Er zijn vele verschillen|Deze koelkast is er|Je kunt hem|Dit is er een} in diverse uitvoeringen Kopen. <strong>#{product.title} #{product.category.name}</strong>".spin
          spin2 = "{In de|Met de| Dankzij de|Ook met de} #{product.title} {kun je eenvoudig al|makkelijk|zonder problemen|in een oogwenk}  je etenswaren kwijt {Het grote voordeel|wat makkelijk is| Handig}  van een {koelvriescombinatie|koelkast| koel combi} is,".spin
          spin3 = "is dat {je niet vaak|uw zelden| je meestal niet} meer hoeft te bukken om je {etenswaren| producten | dingen} te pakken. {Aan de buitenkant| Van Buitenaf | Van buiten}".spin
          spin4 = "{kun je eenvoudig koud water tappen| Kun je soms water tappen}.</p><p> {Deze koelvriescombinatie|De koelkast | Een koelvriescombi als deze| Dit soort koelkasten} {hoef je niet op een waterleiding aan te sluiten| moeten niet op het water worden aangesloten}.".spin
          spin5 = "{De|Deze} #{product.title} {is een|valt in de categorie van} #{product.refrigerators.last.koelkast_model} en {is een| gaat door voor| vaak benoemd als} #{product.refrigerators.last.soort}.".spin
          spin6 = "{Hij valt in de energieklasse| De energieklasse is| de energieklasse| Deze koelkast heeft energieklasse}    #{product.refrigerators.last.energie_klasse} {ook het geluid valt mee| hij maakt wieling lawaai| dit is geen lawaaierige koekast| deze koelkast is erg stil |deze koelcombine is vrij stil} ".spin
          spin7 = "{ met| dankzij| door de} #{product.refrigerators.last.geluidsniveau} {db|dB|Decibel}.</p><p> {Zoals elke chefkok u kan vertellen|Wat elke kok je kan vertellen|Elke chef kan je vertellen}, {draait het allemaal om presentatie|Prestaties zijn belangrijk|onderschat het belang van prestaties niet|prestaties zijn waar het om draait},".spin
          spin8 = "{vooral in de keuken|met name als het op kopen aankomt| zeker in de keuken| met name met eten}. {De eigentijdse elegantie van de|De moderne uitstraling van de|Het prachtige ontwerp van de| dankzij het design van de}  #{product.title}  {een inspireerde koelkast| koelvriescombinatie die inspireert| voel je je weer geïnspireerd in de keuken}".spin
          spin9 = "{De| Deze} #{product.title} is  #{product.refrigerators.last.breedte} breed {en| ook is hij} #{product.refrigerators.last.hoogte}  cm {hoog|in de hoogte| in de lengte}.".spin
          spin10 = "{Deze koelkast| Deze koelvriescombi|Deze koelvriescombinatie| De koelkast |De koelvriescombinatie| hij is}  #{product.refrigerators.last.diepte} cm in de diepte.</p><p>".spin
          spin11 = "{In de groentelades worden jouw groenten| in de lades word alles|in de koelkast wordt alles| Alles wordt} zo goed mogelijk bewaard. {Op de plateaus is genoeg ruimte voor jouw dagelijkse boodschappen| Er is genoeg ruimte |ruimte is geen probleem}.".spin
          spin12 = "{Voor mooi aanbiedingen van de| Om aanbiedingen van te ontvangen van de| Voor mooie prijzen van de} #{product.title} kun je de prijzen bekijken {bij tal van webshops|bij verschillende webshops| bij webwinkels bij winkels} zoal Bol.com, Mediamarkt en wehkkamp.".spin
          spin13 = "{De koelkast heeft|Deze koelkast heeft | Deze koelcombi heeft| De koelcombi heeft} een koelcapaciteit van #{product.refrigerators.last.inhoud_koelruimte}  liter en {het vriesvak| het vriesgedeelte} heeft een capaciteit van #{product.refrigerators.last.inhoud_vriesruimte} liter.</p>".spin
          product.spin = " #{spin1.sample} #{spin2.sample} #{spin3.sample} #{spin4.sample} #{spin5.sample} #{spin6.sample} #{spin7.sample} #{spin8.sample} #{spin9.sample} #{spin10.sample} #{spin11.sample} #{spin12.sample} #{spin13.sample}"
          product.save
          puts product.spin

         end
      end
    end
  end
end
