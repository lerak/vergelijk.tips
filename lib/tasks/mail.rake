namespace :mail do
  desc "mail"
  task :price_match => :environment do
    customers = Customer.all

    customers.each do |customer|
      if customer.product.min_amount_today.price <= customer.price
         CustomerMailer.price_match(customer).deliver
         puts "sending mail to #{customer.email} customer price: #{customer.price}, min product price: #{customer.product.min_amount_today.price}"
         customer.delete
         puts "Deleting  #{customer.email} "
       end


    end
  end
end
