namespace :update_tradetracker do
  desc "update products"
  task :plasma_discounter => :environment do
    require 'nokogiri'
    require 'open-uri'

    url = "http://pf.tradetracker.net/?aid=260894&encoding=utf-8&type=xml-v2&fid=968474&categoryType=2&additionalType=2"
    doc = Nokogiri::XML(open(url))

    products = Product.all

    products.each do |product|
        doc.xpath("//product").each do |item|
            if product.ean.nil?
              #  puts "No Result......doing Noting....."
              elsif item.text.include?(product.ean)

                  puts item.css("timetoship").text

                  price = item.css("price").text
                  shop = "Plasma Discounter"
                  url = item.css("URL").text
                  product.amounts.create(price: price ,shop: shop).links.create(link: url)

                  puts "adding prices to #{product.title}....."
                end
            end
        end

  end

  desc "update products"
  task :artandcraft => :environment do
    require 'nokogiri'
    require 'open-uri'

    url = "http://pf.tradetracker.net/?aid=260894&encoding=utf-8&type=xml-v2&fid=916100&productFeedCategoryHash=34a6d7e64a715168c34ca8a073ccd244&categoryType=2&additionalType=2"
    doc = Nokogiri::XML(open(url))
    products = Product.all

    products.each do |product|
        doc.xpath("//product").each do |item|
            if product.ean.nil?
              #  puts "No Result......doing Noting....."
              elsif item.text.include?(product.ean)

                  puts item.css("timetoship").text

                  price = item.css("price").text
                  shop = "Art & Craft"
                  url = item.css("URL").text
                  product.amounts.create(price: price ,shop: shop).links.create(link: url)

                  puts "adding prices to #{product.title}....."
                end
            end
        end

  end

  desc "update products"
  task :artandcraft1 => :environment do
    require 'nokogiri'
    require 'open-uri'

    url = "http://pf.tradetracker.net/?aid=260894&encoding=utf-8&type=xml-v2&fid=916100&productFeedCategoryHash=7ff60513567161fccdaa568d043e534e&categoryType=2&additionalType=2"
    doc = Nokogiri::XML(open(url))
    products = Product.all

    products.each do |product|
        doc.xpath("//product").each do |item|
            if product.ean.nil?
              #  puts "No Result......doing Noting....."
              elsif item.text.include?(product.ean)

                  puts item.css("timetoship").text

                  price = item.css("price").text
                  shop = "Art & Craft"
                  url = item.css("URL").text
                  product.amounts.create(price: price ,shop: shop).links.create(link: url)

                  puts "adding prices to #{product.title}....."
                end
            end
        end

  end

  desc "update products"
  task :artandcraft2 => :environment do
    require 'nokogiri'
    require 'open-uri'

    url = "http://pf.tradetracker.net/?aid=260894&encoding=utf-8&type=xml-v2&fid=916100&productFeedCategoryHash=e421eea3bca9d8df25e5331b4466e3b1&categoryType=2&additionalType=2"
    doc = Nokogiri::XML(open(url))
    products = Product.all

    products.each do |product|
        doc.xpath("//product").each do |item|
            if product.ean.nil?
              #  puts "No Result......doing Noting....."
              elsif item.text.include?(product.ean)

                  puts item.css("timetoship").text

                  price = item.css("price").text
                  shop = "Art & Craft"
                  url = item.css("URL").text
                  product.amounts.create(price: price ,shop: shop).links.create(link: url)

                  puts "adding prices to #{product.title}....."
                end
            end
        end

  end


  desc "update products"
  task :ep => :environment do
    require 'nokogiri'
    require 'open-uri'

    url = "http://pf.tradetracker.net/?aid=260894&encoding=utf-8&type=xml-v2&fid=857920&categoryType=2&additionalType=2"
    doc = Nokogiri::XML(open(url))

    products = Product.all

    products.each do |product|
        doc.xpath("//product").each do |item|
            if product.ean.nil?
              #  puts "No Result......doing Noting....."
              elsif item.text.include?(product.ean)

                  # puts item.css("timetoship").text

                  price = item.css("price").text
                  shop = "EP.NL"
                  url = item.css("URL").text
                  product.amounts.create(price: price ,shop: shop).links.create(link: url)

                  puts "adding prices to #{product.title}....."
                end
            end
        end
   end

   desc "update products"
   task :foka => :environment do
     require 'nokogiri'
     require 'open-uri'

     url = "http://pf.tradetracker.net/?aid=260894&encoding=utf-8&type=xml-v2&fid=384729&categoryType=2&additionalType=2"
     doc = Nokogiri::XML(open(url))

     products = Product.all

     products.each do |product|
         doc.xpath("//product").each do |item|
             if product.ean.nil?
               #  puts "No Result......doing Noting....."
               elsif item.text.include?(product.ean)

                   # puts item.css("timetoship").text

                   price = item.css("price").text
                   shop = "FOKA"
                   url = item.css("URL").text
                   product.amounts.create(price: price ,shop: shop).links.create(link: url)

                   puts "adding prices to #{product.title}....."
                 end
             end
         end
    end


    # conrad http://pf.tradetracker.net/?aid=38540&encoding=utf-8&type=xml-v2&fid=250314&categoryType=2&additionalType=2
    desc "update products"
    task :conrad => :environment do
      require 'nokogiri'
      require 'open-uri'

      url = "http://pf.tradetracker.net/?aid=38540&encoding=utf-8&type=xml-v2&fid=250314&categoryType=2&additionalType=2"
      doc = Nokogiri::XML(open(url))

      products = Product.all

      products.each do |product|
          doc.xpath("//product").each do |item|
              if product.ean.nil?
                #  puts "No Result......doing Noting....."
                elsif item.text.include?(product.ean)

                    # puts item.css("timetoship").text

                    price = item.css("price").text
                    shop = "Conrad"
                    url = item.css("URL").text
                    product.amounts.create(price: price ,shop: shop).links.create(link: url)

                    puts "adding prices to #{product.title}....."
                  end
              end
          end
     end
    # coolsound
    desc "update products"
    task :coolsound => :environment do
      require 'nokogiri'
      require 'open-uri'

      url = "http://pf.tradetracker.net/?aid=38540&encoding=utf-8&type=xml-v2&fid=250309&categoryType=2&additionalType=2"
      doc = Nokogiri::XML(open(url))

      products = Product.all

      products.each do |product|
          doc.xpath("//product").each do |item|
              if product.ean.nil?
                #  puts "No Result......doing Noting....."
                elsif item.text.include?(product.ean)

                    # puts item.css("timetoship").text

                    price = item.css("price").text
                    shop = "Coolsound"
                    url = item.css("URL").text
                    product.amounts.create(price: price ,shop: shop).links.create(link: url)

                    puts "adding prices to #{product.title}....."
                  end
              end
          end
     end

    #  fonq begin
    desc "update products"
    task :fonq => :environment do
      require 'nokogiri'
      require 'open-uri'

      url = "http://pf.tradetracker.net/?aid=260894&encoding=utf-8&type=xml-v2&fid=584064&categoryType=2&additionalType=2&part=1_2"
      doc = Nokogiri::XML(open(url))
      products = Product.all

      products.each do |product|
          doc.xpath("//product").each do |item|
              if product.ean.nil?
                #  puts "No Result......doing Noting....."
                elsif item.text.include?(product.ean)

                    puts item.css("timetoship").text

                    price = item.css("price").text
                    shop = "Fonq"
                    url = item.css("URL").text
                    product.amounts.create(price: price ,shop: shop).links.create(link: url)

                    puts "adding prices to #{product.title}....."
                  end
              end
          end

    end
    desc "update products"
    task :fonq_1 => :environment do
      require 'nokogiri'
      require 'open-uri'

      url = "http://pf.tradetracker.net/?aid=260894&encoding=utf-8&type=xml-v2&fid=584064&categoryType=2&additionalType=2&part=2_2"
      doc = Nokogiri::XML(open(url))
      products = Product.all

      products.each do |product|
          doc.xpath("//product").each do |item|
              if product.ean.nil?
                #  puts "No Result......doing Noting....."
                elsif item.text.include?(product.ean)

                    puts item.css("timetoship").text

                    price = item.css("price").text
                    shop = "Fonq"
                    url = item.css("URL").text
                    product.amounts.create(price: price ,shop: shop).links.create(link: url)

                    puts "adding prices to #{product.title}....."
                  end
              end
          end

    end
    # fonq end
    #  caraudioshop
    desc "update products"
    task :caraudioshop => :environment do
      require 'nokogiri'
      require 'open-uri'

      url = "http://pf.tradetracker.net/?aid=260894&encoding=utf-8&type=xml-v2&fid=577528&categoryType=2&additionalType=2"
      doc = Nokogiri::XML(open(url))

      products = Product.all

      products.each do |product|
          doc.xpath("//product").each do |item|
              if product.ean.nil?
                #  puts "No Result......doing Noting....."
                elsif item.text.include?(product.ean)

                    # puts item.css("timetoship").text

                    price = item.css("price").text
                    shop = "Caraudioshop"
                    url = item.css("URL").text
                    product.amounts.create(price: price ,shop: shop).links.create(link: url)

                    puts "adding prices to #{product.title}....."
                  end
              end
          end
     end
end
