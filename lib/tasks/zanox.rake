namespace :update_zanox do
  desc "update products"
  task :plattetv => :environment do
    require 'nokogiri'
    require 'open-uri'

    url = "http://df.zanox.com/download/stream-2001104-2122481.xml?dfid=2001104&asid=2122481&m=xml_tree&cids=BGPQR&itid=3146&token=79F2B1348F6880F3E448D684F17B8B992FB6B4E4D2A9530F0F18D314344856BCC946C0"
    doc = Nokogiri::XML(open(url))

      products = Product.all

    products.each do |product|
        doc.xpath("//record").each do |item|
            if product.ean.nil?
               puts "No Result......doing Noting....."
              elsif item.text.include?(product.ean)

                  puts item.css("timetoship").text

                  price = item.css("price").text
                  shop = "Platte TV"
                  url = item.css("url").text
                  product.amounts.create(price: price ,shop: shop).links.create(link: url)

                  puts "adding prices to #{product.title}....."
            end
        end
    end

  end

  desc "update products"
  task :expert => :environment do
    require 'nokogiri'
    require 'open-uri'

    url = "http://df.zanox.com/download?dfid=2000341&asid=2122481&m=xml_tree&cids=BGQRS&itid=3146&token=79F2B1348F6880F3E448D684F17B8B992FB6B4E4D2A9530F0F18D314344856BCC946C0"
    doc = Nokogiri::XML(open(url))
    products = Product.all


    products.each do |product|
        doc.xpath("//record").each do |item|
            if product.ean.nil?
               puts "No Result......doing Noting....."
              elsif item.text.include?(product.ean)

                  puts item.css("timetoship").text

                  price = item.css("price").text
                  shop = "Expert"
                  url = item.css("url").text
                  product.amounts.create(price: price ,shop: shop).links.create(link: url)

                  puts "adding prices to #{product.title}....."
            end
        end
    end
end
desc "update products"
task :coolblue => :environment do
  require 'nokogiri'
  require 'open-uri'

  url = "http://df.zanox.com/download?dfid=293106&asid=2122481&m=xml_tree&enq=ISO_8859_15&cids=BGR&itid=3162&token=79F2B1348F6880F3E448D684F17B8B992FB6B4E4D2A9530F0F18D314344856BCC946C0"
  doc = Nokogiri::XML(open(url))
  products = Product.all


  products.each do |product|
      doc.xpath("//record").each do |item|
          if product.ean.nil?
             puts "No Result......doing Noting....."
            elsif item.text.include?(product.ean)



                price = item.css("price").text
                shop = "CoolBlue"
                url = item.css("url").text
                product.amounts.create(price: price ,shop: shop).links.create(link: url)

                puts "adding prices to #{product.title}....."
          end
      end
  end
end
desc "update products"
task :bestsellers => :environment do
  require 'nokogiri'
  require 'open-uri'

  url = "http://df.zanox.com/download?dfid=2000105&asid=2122481&m=xml_tree&itid=3152&token=79F2B1348F6880F3E448D684F17B8B992FB6B4E4D2A9530F0F18D314344856BCC946C0"
  doc = Nokogiri::XML(open(url))
  products = Product.all


  products.each do |product|
      doc.xpath("//record").each do |item|
          if product.ean.nil?
             puts "No Result......doing Noting....."
            elsif item.text.include?(product.ean)

                puts item.css("timetoship").text

                price = item.css("price").text
                shop = "Bestsellers.eu"
                url = item.css("url").text
                product.amounts.create(price: price ,shop: shop).links.create(link: url)

                puts "adding prices to #{product.title}....."
          end
      end
  end
end
end
