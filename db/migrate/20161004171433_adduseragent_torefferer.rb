class AdduseragentTorefferer < ActiveRecord::Migration[5.0]
  def change
    add_column :referers, :user_agent, :string
  end
end
