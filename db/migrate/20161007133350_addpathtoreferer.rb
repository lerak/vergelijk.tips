class Addpathtoreferer < ActiveRecord::Migration[5.0]
  def change
    add_column :referers, :path, :string
  end
end
