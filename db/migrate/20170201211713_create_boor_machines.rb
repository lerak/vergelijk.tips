class CreateBoorMachines < ActiveRecord::Migration[5.0]
  def change
    create_table :boor_machines do |t|
      t.integer :toeren_per_minuut
      t.integer :soort_boorhouder
      t.integer :klopfunctie
      t.string  :gewicht
      t.string :koppel
      t.string :vermogen
      t.references :product, foreign_key: true
      t.timestamps
    end
  end
end
