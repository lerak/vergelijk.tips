class CreateBullets < ActiveRecord::Migration[5.0]
  def change
    create_table :bullets do |t|
      t.string :title
      t.string :description
      t.references :product, foreign_key: true

      t.timestamps
    end
  end
end
