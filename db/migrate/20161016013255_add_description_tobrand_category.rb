class AddDescriptionTobrandCategory < ActiveRecord::Migration[5.0]
  def change
    add_column :brands, :description, :text
    add_column :categories, :description, :text
  end
end
