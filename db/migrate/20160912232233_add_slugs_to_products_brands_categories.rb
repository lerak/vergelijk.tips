class AddSlugsToProductsBrandsCategories < ActiveRecord::Migration[5.0]
  def change
    add_column :products, :slug, :string
    add_index :products, :slug
    add_column :categories, :slug, :string
    add_index :categories, :slug
    add_column :brands, :slug, :string
    add_index :brands, :slug
  end
end
