class CreateAmounts < ActiveRecord::Migration[5.0]
  def change
    create_table :amounts do |t|
      t.float :price
      t.references :product, foreign_key: true

      t.timestamps
    end
  end
end
