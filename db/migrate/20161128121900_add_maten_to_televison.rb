class AddMatenToTelevison < ActiveRecord::Migration[5.0]
  def change
    add_column :televisons, :hoogte, :string
    add_column :televisons, :breedte, :string
    add_column :televisons, :diepte, :string
    add_column :televisons, :gewicht, :string
    add_column :televisons, :wifi, :boolean
    add_column :televisons, :hdmi, :boolean
    add_column :televisons, :usb, :boolean
  end
end
