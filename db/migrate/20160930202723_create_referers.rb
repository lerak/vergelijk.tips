class CreateReferers < ActiveRecord::Migration[5.0]
  def change
    create_table :referers do |t|
      t.string :url

      t.timestamps
    end
  end
end
