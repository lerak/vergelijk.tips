class CreateRefrigerators < ActiveRecord::Migration[5.0]
  def change
    create_table :refrigerators do |t|
      t.integer :koelkast_model
      t.integer :energie_klasse
      t.integer :soort
      t.string :inhoud_koelruimte
      t.string :inhoud_vriesruimte
      t.string :geluidsniveau
      t.string :breedte
      t.string :hoogte
      t.string :diepte
      t.string :gewicht
      t.boolean :no_frost
      t.boolean :low_frost
      t.boolean :automatisch_ontdooien
      t.boolean :snel_invriezen
      t.boolean :ijdispenser
      t.boolean :open_deur_alarm
      t.boolean :superkoelen
      t.boolean :draaideur_richting_aanpasbaar
      t.boolean :temperatuur_instelbaar
      t.references :product, foreign_key: true
      t.timestamps
    end
  end
end
