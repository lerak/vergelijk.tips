class CreateAutoRadios < ActiveRecord::Migration[5.0]
  def change
    create_table :auto_radios do |t|
      t.string :vermogen
      t.integer :plaatsing
      t.integer :type_speaker
      t.references :product, foreign_key: true
      t.timestamps
    end
  end
end
