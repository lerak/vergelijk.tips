class AddUrlWebshopnameToamounts < ActiveRecord::Migration[5.0]
  def change
    add_column :amounts, :url, :string
    add_index :amounts, :url
    add_column :amounts, :shop, :string
    add_index :amounts, :shop
    add_index :amounts, :price
  end
end
