class CreateEspressoMachines < ActiveRecord::Migration[5.0]
  def change
    create_table :espresso_machines do |t|
      t.string :inhoud
      t.string :gewicht
      t.string :lengte
      t.string :breedte
      t.string :hoogte
      t.integer :plaatsing
      t.integer :gemalen_koffie
      t.integer :koffiebonen
      t.integer :cups_of_capsules
      t.integer :pads_of_servings
      t.integer :maakt_cappuccino
      t.integer :maakt_espresso
      t.integer :maakt_normale_koffie
      t.references :product, foreign_key: true
      t.timestamps
    end
  end
end
