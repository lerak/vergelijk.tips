class AddMetadescriptiontoproducts < ActiveRecord::Migration[5.0]
  def change
    add_column :products, :short_description, :string
    add_column :products, :meta_description, :string
    add_column :products, :keyword, :string
    add_column :categories, :meta_description, :string
    add_column :categories, :keyword, :string
    add_column :brands, :meta_description, :string
    add_column :brands, :keyword, :string
  end
end
