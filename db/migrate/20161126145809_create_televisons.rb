class CreateTelevisons < ActiveRecord::Migration[5.0]
  def change
    create_table :televisons do |t|
      t.integer :beeldscherm_grote
      t.integer :resolutie
      t.boolean :smart_tv
      t.integer :schermtype
      t.integer :connections_value
      t.integer :energie_klasse
      t.integer :soort_decoder
      t.boolean :drie_d
      t.boolean :curved
      t.integer :surround_systeem
      t.integer :processor
      t.boolean :ci
      t.integer :aantal_speakers
      t.references :product, foreign_key: true
      t.timestamps
    end
  end
end
