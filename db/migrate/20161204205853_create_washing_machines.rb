class CreateWashingMachines < ActiveRecord::Migration[5.0]
  def change
    create_table :washing_machines do |t|
      t.integer :energie_klasse
      t.string :geluidsniveau
      t.string :breedte
      t.string :hoogte
      t.string :diepte
      t.string :gewicht
      t.integer :belading
      t.integer :bouw
      t.boolean :wasdroger
      t.integer :maximum_vulgewicht
      t.integer :toeren_per_minuut
      t.boolean :display
      t.boolean :resttijd_indicatie
      t.boolean :starttijd_keuze
      t.boolean :waterlekage_beveiliging
      t.boolean :intergreerbaar
      
      t.references :product, foreign_key: true
      t.timestamps
    end
  end
end
