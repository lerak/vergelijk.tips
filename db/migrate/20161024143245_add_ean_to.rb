class AddEanTo < ActiveRecord::Migration[5.0]
  def change
    add_column :products, :ean, :string
  end
end
