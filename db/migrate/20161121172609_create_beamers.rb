class CreateBeamers < ActiveRecord::Migration[5.0]
  def change
    create_table :beamers do |t|
      t.references :product, foreign_key: true
      t.string :resolutie
      t.string :lumen
      t.string :aansluitingen
      t.string :geluidsniveau
      t.boolean :drie_d
      t.boolean :speaker
      t.string :gewicht
      t.string :diepte
      t.string :hoogte
      t.string :breedte

      t.timestamps
    end
  end
end
