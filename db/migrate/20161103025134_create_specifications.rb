class CreateSpecifications < ActiveRecord::Migration[5.0]
  def change
    create_table :specifications do |t|
      t.string :label
      t.string :content
      t.integer :type
      t.references :product, foreign_key: true
    end
  end
end
