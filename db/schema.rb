# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170212162841) do

  create_table "amounts", force: :cascade do |t|
    t.float    "price"
    t.integer  "product_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "url"
    t.string   "shop"
    t.index ["price"], name: "index_amounts_on_price"
    t.index ["product_id"], name: "index_amounts_on_product_id"
    t.index ["shop"], name: "index_amounts_on_shop"
    t.index ["url"], name: "index_amounts_on_url"
  end

  create_table "auto_radios", force: :cascade do |t|
    t.string   "vermogen"
    t.integer  "plaatsing"
    t.integer  "type_speaker"
    t.integer  "product_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["product_id"], name: "index_auto_radios_on_product_id"
  end

  create_table "beamers", force: :cascade do |t|
    t.integer  "product_id"
    t.string   "resolutie"
    t.string   "lumen"
    t.string   "aansluitingen"
    t.string   "geluidsniveau"
    t.boolean  "drie_d"
    t.boolean  "speaker"
    t.string   "gewicht"
    t.string   "diepte"
    t.string   "hoogte"
    t.string   "breedte"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["product_id"], name: "index_beamers_on_product_id"
  end

  create_table "boor_machines", force: :cascade do |t|
    t.integer  "toeren_per_minuut"
    t.integer  "soort_boorhouder"
    t.integer  "klopfunctie"
    t.string   "gewicht"
    t.string   "koppel"
    t.string   "vermogen"
    t.integer  "product_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "type_boor"
    t.integer  "accu_boor"
    t.integer  "accu"
    t.index ["product_id"], name: "index_boor_machines_on_product_id"
  end

  create_table "brands", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "slug"
    t.string   "meta_description"
    t.string   "keyword"
    t.text     "description"
    t.index ["slug"], name: "index_brands_on_slug"
  end

  create_table "bullets", force: :cascade do |t|
    t.string   "title"
    t.string   "description"
    t.integer  "product_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["product_id"], name: "index_bullets_on_product_id"
  end

  create_table "categories", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "slug"
    t.string   "meta_description"
    t.string   "keyword"
    t.text     "description"
    t.index ["slug"], name: "index_categories_on_slug"
  end

  create_table "customers", force: :cascade do |t|
    t.string   "email"
    t.string   "name"
    t.integer  "price"
    t.integer  "product_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["product_id"], name: "index_customers_on_product_id"
  end

  create_table "espresso_machines", force: :cascade do |t|
    t.string   "inhoud"
    t.string   "gewicht"
    t.string   "lengte"
    t.string   "breedte"
    t.string   "hoogte"
    t.integer  "plaatsing"
    t.integer  "gemalen_koffie"
    t.integer  "koffiebonen"
    t.integer  "cups_of_capsules"
    t.integer  "pads_of_servings"
    t.integer  "maakt_cappuccino"
    t.integer  "maakt_espresso"
    t.integer  "maakt_normale_koffie"
    t.integer  "product_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.index ["product_id"], name: "index_espresso_machines_on_product_id"
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
    t.index ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id"
    t.index ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type"
  end

  create_table "images", force: :cascade do |t|
    t.string   "title"
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
    t.integer  "product_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.index ["product_id"], name: "index_images_on_product_id"
  end

  create_table "links", force: :cascade do |t|
    t.string   "link"
    t.integer  "amount_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["amount_id"], name: "index_links_on_amount_id"
  end

  create_table "products", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "brand_id"
    t.integer  "category_id"
    t.string   "slug"
    t.string   "short_description"
    t.string   "meta_description"
    t.string   "keyword"
    t.string   "product_model"
    t.string   "ean"
    t.text     "spin"
    t.index ["brand_id"], name: "index_products_on_brand_id"
    t.index ["category_id"], name: "index_products_on_category_id"
    t.index ["slug"], name: "index_products_on_slug"
  end

  create_table "referers", force: :cascade do |t|
    t.string   "url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "ip_address"
    t.string   "user_agent"
    t.string   "path"
  end

  create_table "refrigerators", force: :cascade do |t|
    t.integer  "koelkast_model"
    t.integer  "energie_klasse"
    t.integer  "soort"
    t.string   "inhoud_koelruimte"
    t.string   "inhoud_vriesruimte"
    t.string   "geluidsniveau"
    t.string   "breedte"
    t.string   "hoogte"
    t.string   "diepte"
    t.string   "gewicht"
    t.boolean  "no_frost"
    t.boolean  "low_frost"
    t.boolean  "automatisch_ontdooien"
    t.boolean  "snel_invriezen"
    t.boolean  "ijdispenser"
    t.boolean  "open_deur_alarm"
    t.boolean  "superkoelen"
    t.boolean  "draaideur_richting_aanpasbaar"
    t.boolean  "temperatuur_instelbaar"
    t.integer  "product_id"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.index ["product_id"], name: "index_refrigerators_on_product_id"
  end

  create_table "specifications", force: :cascade do |t|
    t.string  "label"
    t.string  "content"
    t.integer "type"
    t.integer "product_id"
    t.index ["product_id"], name: "index_specifications_on_product_id"
  end

  create_table "televisons", force: :cascade do |t|
    t.integer  "beeldscherm_grote"
    t.integer  "resolutie"
    t.boolean  "smart_tv"
    t.integer  "schermtype"
    t.integer  "connections_value"
    t.integer  "energie_klasse"
    t.integer  "soort_decoder"
    t.boolean  "drie_d"
    t.boolean  "curved"
    t.integer  "surround_systeem"
    t.integer  "processor"
    t.boolean  "ci"
    t.integer  "aantal_speakers"
    t.integer  "product_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "hoogte"
    t.string   "breedte"
    t.string   "diepte"
    t.string   "gewicht"
    t.boolean  "wifi"
    t.boolean  "hdmi"
    t.boolean  "usb"
    t.index ["product_id"], name: "index_televisons_on_product_id"
  end

  create_table "washing_machines", force: :cascade do |t|
    t.integer  "energie_klasse"
    t.string   "geluidsniveau"
    t.string   "breedte"
    t.string   "hoogte"
    t.string   "diepte"
    t.string   "gewicht"
    t.integer  "belading"
    t.integer  "bouw"
    t.boolean  "wasdroger"
    t.integer  "maximum_vulgewicht"
    t.integer  "toeren_per_minuut"
    t.boolean  "display"
    t.boolean  "resttijd_indicatie"
    t.boolean  "starttijd_keuze"
    t.boolean  "waterlekage_beveiliging"
    t.boolean  "intergreerbaar"
    t.integer  "product_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.index ["product_id"], name: "index_washing_machines_on_product_id"
  end

  create_table "works", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["confirmation_token"], name: "index_works_on_confirmation_token", unique: true
    t.index ["email"], name: "index_works_on_email", unique: true
    t.index ["reset_password_token"], name: "index_works_on_reset_password_token", unique: true
    t.index ["unlock_token"], name: "index_works_on_unlock_token", unique: true
  end

end
