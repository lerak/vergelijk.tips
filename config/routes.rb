Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/works_config', as: 'rails_admin'
  devise_for :works
  # mount RailsAdmin::Engine => '/works_config', as: 'rails_admin'
  # devise_for :works

  resources :amounts, only: [:show]do
    resources :links, only: [:show, :index]
  end
get 'links/:id/go' => 'links#go'
get 'contact', to: 'contact#send_mail'
post 'contact', to: 'contact#send_mail'


  resources :referers, only: [:index]
  root 'products#index'
  get 'category/new', to: 'categories#new'
  post 'category/new', to: 'categories#create'
  get 'category/edit/:id', to: 'categories#edit'
  put 'category/edit/:id', to: 'categories#update'
  patch 'category/edit/:id', to: 'categories#update'
  get 'brand/new', to: 'brands#new'
  post 'brand/new', to: 'brands#create'
  get 'brand/edit/:id', to: 'brands#edit'
  put 'brand/edit/:id', to: 'brands#update'
  patch 'brand/edit/:id', to: 'brands#update'
  get 'product/new', to: 'products#new'
  post 'product/new', to: 'products#create'


 resources :products, only: [:edit, :update]

  #resources :products, only: [:new, :create, :edit, :update]
  resources :categories, path: '',only: [:show] do
    resources :brands, path: '', only: [:show] do
      resources :products, path: '',only: [:show] do
        resources :customers, only: [:create]

      end
    end
  end


resources :referers, only: [:index]

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
