# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "http://www.vergelijk.tips"

SitemapGenerator::Sitemap.create do
  # Put links creation logic here.
  #
  # The root path '/' and sitemap index file are added automatically for you.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: add(path, options={})
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
  #
  # Examples:
  # Category.find_each do |category|
  #     add category_path(category)
  #     add  category_brand(category, category.brand)
  # end
  Category.find_each do |category|
    add category_path(category)
  end
  urls = []
  Product.find_each do |product|
      urls << category_brand_path(product.category, product.brand)
  end
  urls = urls.uniq
  urls.each do |url|
    add url
  end


  Product.find_each do |product|
    add category_brand_product_path(product.category, product.brand, product)
  end
  # category_brand_product_path(product.category, product.brand, product)
  #
  #   add articles_path, :priority => 0.7, :changefreq => 'daily'
  #
  # Add all articles:
  #
  #   Article.find_each do |article|
  #     add article_path(article), :lastmod => article.updated_at
  #   end
end
