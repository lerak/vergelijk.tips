# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path
Rails.application.config.assets.precompile += %w( font-size.js )
Rails.application.config.assets.precompile += %w( vendors.js )

Rails.application.config.assets.precompile += %w( shCore.js )
Rails.application.config.assets.precompile += %w( shBrushxml.js )
# Rails.application.config.assets.precompile += %w( shBrushJScript.js )
Rails.application.config.assets.precompile += %w( shBrushJScript.js )

Rails.application.config.assets.precompile += %w( DropdownHover.js )
Rails.application.config.assets.precompile += %w( app.js )
Rails.application.config.assets.precompile += %w( holder.js )
Rails.application.config.assets.precompile += %w( e-commerce_product.js )
# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )
Rails.application.config.assets.precompile += %w( vendors.css )
Rails.application.config.assets.precompile += %w( shCore.css )
Rails.application.config.assets.precompile += %w( style-blue.css )
Rails.application.config.assets.precompile += %w( width-full.css )
