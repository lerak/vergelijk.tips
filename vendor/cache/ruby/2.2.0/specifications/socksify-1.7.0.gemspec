# -*- encoding: utf-8 -*-
# stub: socksify 1.7.0 ruby lib

Gem::Specification.new do |s|
  s.name = "socksify".freeze
  s.version = "1.7.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Stephan Maka".freeze, "Andrey Kouznetsov".freeze, "Christopher Thorpe".freeze, "Musy Bite".freeze, "Yuichi Tateno".freeze, "David Dollar".freeze]
  s.date = "2015-07-31"
  s.email = "stephan@spaceboyz.net".freeze
  s.executables = ["socksify_ruby".freeze]
  s.extra_rdoc_files = ["doc/index.html".freeze, "doc/index.css".freeze, "COPYING".freeze]
  s.files = ["COPYING".freeze, "bin/socksify_ruby".freeze, "doc/index.css".freeze, "doc/index.html".freeze]
  s.homepage = "http://socksify.rubyforge.org/".freeze
  s.rubyforge_project = "socksify".freeze
  s.rubygems_version = "2.6.6".freeze
  s.summary = "Redirect all TCPSockets through a SOCKS5 proxy".freeze

  s.installed_by_version = "2.6.6" if s.respond_to? :installed_by_version
end
