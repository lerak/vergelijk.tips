class BoorMachine < ApplicationRecord
  belongs_to :product
  enum toeren_per_minuut: {"1200": 0, "2000": 1, "3000": 2}
  enum soort_boorhouder: {"Snelspan": 0, "SDS Plus": 1, "SDS Max": 2}
  enum klopfunctie: {"Ja": 0, "Nee": 1}
  enum type_boor: {"Boorhammer": 0, "klopboormachine": 1, "Boormachine": 2}
  enum accu: {"Accuboor": 0, "Geen Accuboor": 1}
  enum accu_boor: {"Geen Accu": 0, "LI-ION ACCU": 1, "NICD ACCU": 2}
end
