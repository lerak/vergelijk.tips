class Referer < ApplicationRecord
  scope :googlebot, -> {
      where(['user_agent LIKE ?', "%Googlebot%"])}

    scope :today, -> {
      where("created_at >= ?", Time.zone.now.beginning_of_day)
    }
   scope :visitors_google, -> {
     where(['url LIKE ?', "%google%"])
   }
  scope :clicked_links, -> {
    where(['path LIKE ?', "%amounts%"])
  }

end
