class Category < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged
  has_many :products
  has_many :brands, through: :products

  validates :name, uniqueness: true
  scope :min_price, -> {where(self.product.price < 200)}
end
