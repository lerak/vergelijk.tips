class Product < ApplicationRecord
  extend FriendlyId
  friendly_id :title, use: :slugged
  validates :title, presence: true

  validates :ean, uniqueness: true

  belongs_to :category
  belongs_to :brand
  has_many :images, dependent: :destroy
  has_many :amounts, dependent: :destroy
  has_many :bullets, dependent: :destroy
  has_many :customers  #Email met prijs waarschuwing
  has_many :specifications, dependent: :destroy
# product types categories
  has_many :beamers
  has_many :televisons
  has_many :refrigerators
  has_many :washing_machines
  has_many :boor_machines
  has_many :espresso_machines
  has_many :auto_radios

  accepts_nested_attributes_for :bullets
  accepts_nested_attributes_for :specifications
  accepts_nested_attributes_for :beamers
  accepts_nested_attributes_for :televisons
  accepts_nested_attributes_for :refrigerators
  accepts_nested_attributes_for :washing_machines
  accepts_nested_attributes_for :boor_machines
  accepts_nested_attributes_for :espresso_machines
  accepts_nested_attributes_for :auto_radios



  def amounts_today # prijzen van vandaag!
      self.amounts.where("created_at >= ?", Time.zone.now.beginning_of_day)
  end

  def unique_shops # unique shops 1 bol, coolblue, wehkamp per product.
    if self.amounts_today.any?
      self.amounts_today.order(:shop).to_a.uniq do |x|
      x.shop
      x.url
      x.price
    end
  else
    self.amounts.order(:price).to_a.uniq do |x|
      x.shop
      x.url
      x.price
    end
  end
  end

  def min_amount_today #min Prijs van vandaag
    if self.amounts_today.any?
       self.amounts_today.order(:price).first
    else
       self.amounts.order(:price).first
    end
  end
 def min_amount
   if self.amounts_today.any?
    if  self.amounts.order(:price).first.price < self.amounts_today.order(:price).first.price
      self.amounts.order(:price).first
    else
      nil
    end
  end
 end

 def price_difference
   if self.amounts_today.any?
     if self.amounts.where("created_at >= ?", Time.zone.now.yesterday).order(:price).first.price == self.amounts.where("created_at >= ?", Time.zone.now.beginning_of_day).order(:price).first.price
       false
     elsif self.amounts.where("created_at >= ?", Time.zone.now.yesterday).order(:price).first.price < self.amounts.where("created_at >= ?", Time.zone.now.beginning_of_day).order(:price).first.price
       nr1 = self.amounts.where("created_at >= ?", Time.zone.now.yesterday).order(:price).first.price / 100
       n2 = nr1 * self.amounts.where("created_at >= ?", Time.zone.now.beginning_of_day).order(:price).first.price
       return n2 - self.amounts.where("created_at >= ?", Time.zone.now.yesterday).order(:price).first.price
     else
       false
     end
   end
 end

def self.search(search)
  where("ean LIKE ?", "%#{search}%")
end



# amounts.where("created_at >= ?", Time.zone.now.beginning_of_day)
end



# prices = []
# if self.amounts.where("created_at >= ?", Time.zone.now.beginning_of_day).any?
# self.amounts.where("created_at >= ?", Time.zone.now.beginning_of_day).each do |amount|
#   prices << amount
# end
# else
#   self.amounts.each do |amount|
#     prices << amount
#   end
# end
# return prices.min
