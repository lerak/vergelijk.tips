class WashingMachine < ApplicationRecord
  belongs_to :product
  enum energie_klasse: {"A+++": 0, "A++":1, "A+":2, "A":3, "B": 4, "C": 5, "D": 6}
  enum belading: {"Voorlader": 0, "Bovenlader":1 }
  enum bouw: {"Vrijstaand": 0, "Inbouw": 1, "Onderbouw": 2}
  enum maximum_vulgewicht: {"1": 0, "2": 1, "3": 2, "4": 3, "5": 4, "6": 5, "7": 6, "8": 7, "9": 8, "10": 9, "11": 10, "12": 11, "15": 12}
  enum toeren_per_minuut: {"1000": 0, "1200": 1, "1400": 2, "1600": 3, "1800": 4}
end
