class Brand < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged
  has_many :products
  has_many :categories, through: :products

  validates :name, uniqueness: true
end
