class AutoRadio < ApplicationRecord
  belongs_to :product

  enum plaatsing: [:Inbouw, :Opbouw]
  enum type_speaker: [:"1-weg", :"2-weg", :"3-weg" ]
end
