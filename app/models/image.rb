class Image < ApplicationRecord
  belongs_to :product, dependent: :destroy
  has_attached_file :picture, styles: {
    thumb: '50x50#',
    square: '210x210#',
    medium: '700x700#'
  }

  # Validate the attached image is image/jpg, image/png, etc
  validates_attachment_content_type :picture, :content_type => /\Aimage\/.*\Z/
end
# This method associates the attribute ":avatar" with a file attachment



 # has_attached_file :picture,
 #   styles: {
 #      medium: "3000x3000#",
 #      thumb: "300x250#"
 #    },
 #   :path => "/public/images/:id/:filename",
 #   :url  => ":s3_domain_url",
 #   :storage => :s3,
 #   :default_url => "/default.default",



 # validates_attachment_content_type :picture, :content_type => /\Aimage\/.*\Z/
