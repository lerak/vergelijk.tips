class Televison < ApplicationRecord
  include AttrBitwise
  belongs_to :product

  enum beeldscherm_grote: {
                               "42ince (107cm)": 0,
                               "20 Ince (50cm)": 1,
                               "24 inch (61 cm) ": 2,
                               "22 inch (56 cm)": 3,
                               "32 inch (81 cm)": 4,
                               "43 inch (109 cm)": 5,
                               "49 inch (124 cm)": 6,
                               "55 inch (139 cm)": 7,
                               "84 inch ( 213 cm)": 8,
                               "65 inch (165 cm)": 9,
                               "60 inch (152 cm)": 10,
                               "77 inch (195 cm)": 11,
                               "75 inch (190 cm)": 12,
                               "86 inch (218 cm)": 13,
                               "79 inch (200 cm)": 14,
                               "40 inch (102 cm)": 15,
                               "50 inch (127 cm)": 16,
                               "48 inch (121 cm)": 17,
                               "58 inch (146 cm)": 18,
                               "19 inch (48 cm)": 19,
                               "28 inch (71 cm)": 20,
                               "85 inch (216 cm)": 21,
                               "78 inch (198 cm)": 22,
                               "88 inch (223 cm)": 23
                            }
  enum resolutie: { "1280x720 HD": 0, "1366x768 WXGA": 1, "1600x900 HD+": 2, "1920x1080 FHD": 3, "3840x2160 UHD": 4 }
  enum schermtype: {led: 0, lcd: 1, ips: 2}
  enum energie_klasse: {"A++": 0, "A+": 1, "A": 3, "B":4}
  enum surround_systeem: {"Dolby Digital": 0, "Dolby Digital plus": 1, DTS: 2}
  enum processor: { "Single Core": 0, "Dual Core": 1, "Quad Core": 2, "Octa Core": 3}
  enum aantal_speakers: {"1": 0, "2": 1, "3": 2, "4": 3, "5": 4, "6": 5, "7":6}
  enum soort_decoder: {"Kabel": 0, "Digitenne": 1, "Sateliet": 2}
  attr_bitwise :connections, mapping: [:wifi, :bluetooth, :nfc]
end
