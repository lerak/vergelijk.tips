class Bullet < ApplicationRecord
  belongs_to :product
  #belongs_to :product, counter_cache: true
  validates_associated :product
end
