class Refrigerator < ApplicationRecord
  belongs_to :product

  enum koelkast_model: {"Koel-VriesCombinatie 2 Deurs": 0, "Kastmodel": 1, "Tafelmodel": 2, "Barmodel": 3, "Amerikaans Model": 4}
  enum energie_klasse: {"A++": 0, "A+": 1, "A": 3, "B":4, "C": 5, "D": 6, "A+++": 7}
  enum soort: {"Inbouw": 0, "Vrijstaand": 1, "Semi-Inbouw": 2}
end
