class EspressoMachine < ApplicationRecord
  belongs_to :product
  enum plaatsing: {"Vrijstaand": 0, "Inbouw": 1}
  enum gemalen_koffie: [:ja, :nee], _suffix: true
  enum koffiebonen: [:ja, :nee], _suffix: true
  enum cups_of_capsules: [:ja, :nee], _suffix: true
  enum pads_of_servings: [:ja, :nee], _suffix: true
  enum maakt_cappuccino: [:ja, :nee], _suffix: true
  enum maakt_espresso: [:ja, :nee], _suffix: true
  enum maakt_normale_koffie: [:ja, :nee], _suffix: true
end
