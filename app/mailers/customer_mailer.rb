class CustomerMailer < ActionMailer::Base
  def price_match(customer)
    @customer = customer
    @product = customer.product
    @url = "http://www.vergelijk.tips/#{@customer.product.category.slug}/#{@customer.product.brand.slug}/#{@customer.product.slug}"
   mail(
   to: customer.email,
   from: "prijsbereikt@vergelijk.tips",
   subject: customer.product.title

   )
  end


end
