class CustomersController < ApplicationController
  def create
    @product = Product.friendly.find(params[:product_id])
    @customer = @product.customers.create(customer_params)
    redirect_to :back
  end

private
  def customer_params
    params.require(:customer).permit(:email, :price, :name)
  end
end
