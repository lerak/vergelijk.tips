class ReferersController < ApplicationController
  before_action :authenticate_work!, only: [:index]
  def index
    @referers = Referer.all
    @referers = Referer.all
    @today = Referer.today
    @google = Referer.visitors_google
    @googlebot = Referer.googlebot
    @clicked_links = Referer.clicked_links
  end
end
