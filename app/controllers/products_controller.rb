class ProductsController < ApplicationController
  before_action :authenticate_work!, only: [:new,:create, :edit, :update]
  def index
    @products = Product.order(created_at: :desc).limit(9)
    @categories = Category.all
    if params[:search]
    @products = Product.search(params[:search]).order("created_at DESC")
  else
    @products = Product.order(created_at: :desc).limit(9)
  end
    referers
  end

  def show
    @product = Product.friendly.find(params[:id])
    referers
  end

  def new
    @product = Product.new

  end

  def create
    @product = Product.new(params_product)
    if @product.save
      if params[:pictures]
        #===== The magic is here ;)
        params[:pictures].each { |image|
          @product.images.create(picture: image)
        }
      end
      @product.short_description = "#{@product.title} Kopen? <br>
                                  Bij Vergelijk.tips Kunt uw de prijzen vergelijken.
                                   wij controleren de prijzen van vele webshops."
      @product.meta_description = "vergelijk de prijzen van de #{@product.title} bij verschillende webshops.
                                  De juiste prijs vind je op VERGELIJK.TIPS ✅ veel webshops ✅ laagste prijzen ✅ bespaar geld."
      @product.save
      # @product.amounts.create(price: 0.0, url: "/", shop: "Onbekend").links.create(link: "/")
      redirect_to edit_product_path(@product)
    else
      render :new
    end
  end
 def edit
   @product = Product.friendly.find(params[:id])
   @product.specifications.build

   specs


   bullets
 end

def update
   @product = Product.friendly.find(params[:id])

   if @product.update(params_product)
     if params[:pictures]
       params[:pictures].each { |image|
         @product.images.create(picture: image)
       }
     end
     redirect_to category_brand_product_path(@product.category, @product.brand, @product)
   end
end


private


  def bullets
    if @product.bullets.count < 4
      @product.bullets.build
    end
  end

  def referers
   if request.referer.nil? || !request.referer.include?("/assets")
     Referer.create(url: request.referer, ip_address: request.remote_ip, user_agent: request.user_agent, path: request.path_info)
   end
  end

  # def delete_images
  #   params[:pictures_ids].each do |img|
  #         @product.images.find_by(image_id: img).destroy
  #   end
  # end

  def specs
    case
    when @product.category.name.downcase == "beamers" && @product.beamers.count < 1
         @product.beamers.build
    when  @product.category.name.downcase == "tv" && @product.televisons.count < 1
          @product.televisons.build
    when  @product.category.name.downcase == "koelkasten" &&  @product.refrigerators.count < 1
          @product.refrigerators.build
    when @product.category.name.downcase == "wasmachines" &&  @product.washing_machines.count < 1
         @product.washing_machines.build
   when  @product.category.name.downcase == "boormachines" && @product.boor_machines.count < 1
         @product.boor_machines.build
   when  @product.category.name.downcase == "espressomachine" && @product.espresso_machines.count < 1
         @product.espresso_machines.build
   when  @product.category.name.downcase == "autospeakers" && @product.auto_radios.count < 1
         @product.auto_radios.build
      else

      end
 end

 def params_product
     params.require(:product).permit(:product_model, :short_description, :keyword, :meta_description, :title, :description, :ean, :category_id, :brand_id, pictures_ids: [],
                                        bullets_attributes: [
                                                             :id,
                                                             :title,
                                                             :description
                                                             ],
                                 specifications_attributes: [
                                                             :id,
                                                             :content,
                                                             :label,
                                                             :type
                                                             ],
                                     beamers_attributes: [
                                                          :id,
                                                          :resolutie,
                                                          :lumen,
                                                          :aansluitingen,
                                                          :geluidsniveau,
                                                          :drie_d,
                                                          :speaker,
                                                          :gewicht,
                                                          :diepte,
                                                          :hoogte,
                                                          :breedte
                                                          ],
                                     televisons_attributes: [
                                                             :id,
                                                             :beeldscherm_grote,
                                                             :resolutie,
                                                             :schermtype,
                                                             :energie_klasse,
                                                             :surround_systeem,
                                                             :processor,
                                                             :aantal_speakers,
                                                             :connections,
                                                             :ci,
                                                             :drie_d,
                                                             :smart_tv,
                                                             :curved,
                                                             :soort_decoder,
                                                             :hoogte,
                                                             :breedte,
                                                             :diepte,
                                                             :gewicht,
                                                             :wifi,
                                                             :hdmi,
                                                             :usb
                                                           ],
                                     refrigerators_attributes: [
                                                                :id,
                                                                :koelkast_model,
                                                                :energie_klasse,
                                                                :soort,
                                                                :inhoud_koelruimte,
                                                                :inhoud_vriesruimte,
                                                                :geluidsniveau,
                                                                :breedte,
                                                                :hoogte,
                                                                :diepte,
                                                                :gewicht,
                                                                :no_frost,
                                                                :low_frost,
                                                                :automatisch_ontdooien,
                                                                :snel_invriezen,
                                                                :ijdispenser,
                                                                :open_deur_alarm,
                                                                :superkoelen,
                                                                :draaideur_richting_aanpasbaar,
                                                                :temperatuur_instelbaar
                                                               ],
                                   washing_machines_attributes: [:id,
                                                                :energie_klasse,
                                                                :geluidsniveau,
                                                                :breedte,
                                                                :hoogte,
                                                                :diepte,
                                                                :gewicht,
                                                                :belading,
                                                                :bouw,
                                                                :wasdroger,
                                                                :maximum_vulgewicht,
                                                                :toeren_per_minuut,
                                                                :display,
                                                                :resttijd_indicatie,
                                                                :starttijd_keuze,
                                                                :waterlekage_beveiliging,
                                                                :intergreerbaar
                                                                ],
                                      boor_machines_attributes:[
                                                                :id,
                                                                :gewicht,
                                                                :toeren_per_minuut,
                                                                :soort_boorhouder,
                                                                :klopfunctie,
                                                                :koppel,
                                                                :vermogen,
                                                                :type_boor,
                                                                :accu_boor,
                                                                :accu
                                                                  ],
                                    espresso_machines_attributes:[
                                                                  :id,
                                                                  :gewicht,
                                                                  :inhoud,
                                                                  :lengte,
                                                                  :breedte,
                                                                  :hoogte,
                                                                  :plaatsing,
                                                                  :gemalen_koffie,
                                                                  :koffiebonen,
                                                                  :cups_of_capsules,
                                                                  :pads_of_servings,
                                                                  :maakt_cappuccino,
                                                                  :maakt_espresso,
                                                                  :maakt_normale_koffie
                                                                    ],
                                          auto_radios_attributes: [
                                                                    :id,
                                                                    :vermogen,
                                                                    :plaatsing,
                                                                    :type_speaker
                                                                      ])
 end

end
