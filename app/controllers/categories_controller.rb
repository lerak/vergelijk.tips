class CategoriesController < ApplicationController
  before_action :authenticate_work!, only: [:new,:create]
  def show
    @category = Category.friendly.find(params[:id])
  end

  def new
    @category = Category.new
  end

  def create
    @category = Category.new(params_category)
    if @category.save
      redirect_to category_path(@category)
    else
      render :new
    end
  end

  def edit
    @category = Category.friendly.find(params[:id])
  end

  def update
    @category = Category.friendly.find(params[:id])
     if @category.update(params_category)
      redirect_to category_path(@category)
     else
      render :edit
    end
  end
  private

  def params_category
    params.require(:category).permit!
 end
 def referers
  if request.referer.nil? || !request.referer.include?("/assets")
    Referer.create(url: request.referer, ip_address: request.remote_ip, user_agent: request.user_agent, path: request.path_info)
  end
 end
end
