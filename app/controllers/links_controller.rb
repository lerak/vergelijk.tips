class LinksController < ApplicationController
  def show
    @link = Link.find(params[:id])
    render :layout => false
    referers
    # redirect_to @link.link
  end

  def go
    @link = Link.find(params[:id])
    # link = URI::encode(@link.link)
     redirect_to @link.link
  end

private
  def referers
    if request.referer.nil? || !request.referer.include?("/assets")
      Referer.create(url: request.referer, ip_address: request.remote_ip, user_agent: request.user_agent, path: request.path_info)
    end
  end
end
