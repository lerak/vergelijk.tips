class BrandsController < ApplicationController
 before_action :authenticate_work!, only: [:new,:create]
  def show
    @brand = Brand.friendly.find(params[:id])
  end

  def new
    @brand = Brand.new
  end

  def create
    @brand = Brand.new(params_brand)
    if @brand.save
      redirect_to :back
    else
      render :new
    end
  end


  def edit
    @brand = Brand.friendly.find(params[:id])
  end

  def update
    @brand = Brand.friendly.find(params[:id])
   if @brand.update(params_brand)
      redirect_to root_path
   else
    render :edit
    end
  end

  private

  def params_brand
    params.require(:brand).permit!
 end

 def referers
  if request.referer.nil? || !request.referer.include?("/assets")
    Referer.create(url: request.referer, ip_address: request.remote_ip, user_agent: request.user_agent, path: request.path_info)
  end
 end
end
